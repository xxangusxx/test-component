import { LitElement, html } from "lit-element";

class TestComponent extends LitElement{

    static get properties(){
        return {
            msg: {type: String}
        }
    }

    constructor(){
        super();
        this.msg = 'welcome home boy'
        console.log('default message component: ', this.msg)
    }

    render(){
        return html`
            <div class="test-component">
                <h3>this is my component</h3>
                <h5>${this.msg}</h5>
            </div>
        `;
    }

}

// customElements.define('app-test-component', TestComponent);
export default TestComponent