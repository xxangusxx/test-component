const Html = require('html-webpack-plugin')

module.exports = {
    entry: './src/index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'index.js'
    },
    // module: {
    //     rules: [
    //         {
    //             test: /\.css|\.s(c|a)ss$/,
    //             use: [{
    //                 loader: 'lit-scss-loader',
    //                 options: {
    //                     minify: true, // defaults to false
    //                 },
    //             }, 'extract-loader', 'css-loader', 'sass-loader'],
    //         }
    //     ]
    // },
    plugins: [
        new Html({
            template: 'src/index.html'
        })
    ]
}